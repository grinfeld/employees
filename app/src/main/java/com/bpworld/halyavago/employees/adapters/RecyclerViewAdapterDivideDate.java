package com.bpworld.halyavago.employees.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bpworld.halyavago.employees.DateUtils;
import com.bpworld.halyavago.employees.PicassoUtil;
import com.bpworld.halyavago.employees.R;
import com.bpworld.halyavago.employees.model.OperationItem;
import com.squareup.picasso.Picasso;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by GrinfeldRA
 */

public class RecyclerViewAdapterDivideDate extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "#RecyclerDivideDate";
    @NonNull
    private List<ListItem> items = Collections.emptyList();
    private Activity context;
    private LayoutInflater mInflater;

    public RecyclerViewAdapterDivideDate(@NonNull List<ListItem> items, Activity context) {
        this.items = items;
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ListItem.TYPE_HEADER: {
                View itemView = mInflater.inflate(R.layout.recyclerview_row_operations_header, parent, false);
                return new HeaderViewHolder(itemView);
            }
            case ListItem.TYPE_BODY: {
                View itemView = mInflater.inflate(R.layout.recycler_row_discount, parent, false);
                return new BodyViewHolder(itemView);
            }
            default:
                throw new IllegalStateException("unsupported item type");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case ListItem.TYPE_HEADER: {
                HeaderItem header = (HeaderItem) items.get(position);
                HeaderViewHolder viewHolder = (HeaderViewHolder) holder;
                viewHolder.txt_header.setText(DateUtils.formatDate(header.getDate()));
            }
            break;
            case ListItem.TYPE_BODY: {
                final BodyItem bodyItem = (BodyItem) items.get(position);
                BodyViewHolder viewHolder = (BodyViewHolder) holder;
                viewHolder.txt_name.setText(bodyItem.getOperationItem().getTxt_name());
                viewHolder.txt_time.setText(bodyItem.getOperationItem().getTxt_date());
                Picasso picasso = PicassoUtil.preparePicasso(context);
                if (picasso != null) {
                    picasso.load(bodyItem.getOperationItem().getProfile_image()).into(viewHolder.profile_image);
                }
                viewHolder.txt_disc.setText(bodyItem.getOperationItem().getTxt_disc());
                viewHolder.txt_total.setText(bodyItem.getOperationItem().getTxt_total());
            }
            break;
            default:
                throw new IllegalStateException("unsupported item type");
        }
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView txt_header;

        HeaderViewHolder(View itemView) {
            super(itemView);
            txt_header = itemView.findViewById(R.id.txt_header);
        }

    }

    private class BodyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_name, txt_time, txt_disc, txt_total;
        CircleImageView profile_image;

        BodyViewHolder(final View itemView) {
            super(itemView);
            txt_name = itemView.findViewById(R.id.txt_name);
            txt_time = itemView.findViewById(R.id.txt_time);
            profile_image = itemView.findViewById(R.id.profile_image);
            txt_disc = itemView.findViewById(R.id.txt_disc);
            txt_total = itemView.findViewById(R.id.txt_total);
        }


    }

    public static abstract class ListItem {

        static final int TYPE_HEADER = 0;
        static final int TYPE_BODY = 1;

        abstract public int getType();
    }

    public static class HeaderItem extends ListItem {

        @NonNull
        private Date date;

        public HeaderItem(@NonNull Date date) {
            this.date = date;
        }

        @NonNull
        Date getDate() {
            return date;
        }

        @Override
        public int getType() {
            return TYPE_HEADER;
        }

    }

    public static class BodyItem extends ListItem {

        @NonNull
        private OperationItem operationItem;

        public BodyItem(@NonNull OperationItem operationItem) {
            this.operationItem = operationItem;
        }

        @NonNull
        public OperationItem getOperationItem() {
            return operationItem;
        }

        @Override
        public int getType() {
            return TYPE_BODY;
        }

    }

}
