package com.bpworld.halyavago.employees.view;

import android.Manifest;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.bpworld.halyavago.employees.App;
import com.bpworld.halyavago.employees.PicassoUtil;
import com.bpworld.halyavago.employees.R;
import com.bpworld.halyavago.employees.adapters.RecyclerViewAdapterDiscount;
import com.bpworld.halyavago.employees.adapters.RecyclerViewAdapterDivideDate;
import com.bpworld.halyavago.employees.adapters.RecyclerViewAdapterOperations;
import com.bpworld.halyavago.employees.components.CustomDialogFragment;
import com.bpworld.halyavago.employees.model.Discount;
import com.bpworld.halyavago.employees.model.InfoCashier;
import com.bpworld.halyavago.employees.model.InfoOperations;
import com.bpworld.halyavago.employees.model.OperationItem;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bpworld.halyavago.employees.view.MainActivity.REQUEST_CODE_ADD_DISCOUNT;

/**
 * Created by GrinfeldRA
 */

public class ProfileActivity extends AppCompatActivity implements CustomDialogFragment.IOnActionClickDialog, DatePickerFragment.ICallbacks {

    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final String TAG = "#ProfileActivity";
    private static final int SELECT_PHOTO = 111;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 101;
    private Handler handler;

    @BindView(R.id.profile_image)
    CircleImageView circleImageView;
    @BindView(R.id.txt_name)
    TextView txtFio;
    @BindView(R.id.point_name)
    TextView txtCompany;
    @BindView(R.id.txt_login)
    TextView txtLogin;
    @BindView(R.id.circle)
    View circle;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.txt_today_disc_count)
    TextView txt_today_disc_count;
    @BindView(R.id.txt_today_disc)
    TextView txt_today_disc;
    @BindView(R.id.txt_today_disc_amount)
    TextView txt_today_disc_amount;
    @BindView(R.id.txt_today_amount)
    TextView txt_today_amount;
    @BindView(R.id.view_not_disc)
    View view_not_disc;
    @BindView(R.id.txt_not_disc)
    TextView txt_not_disc;
    @BindView(R.id.btn_add_disc)
    View btn_add_disc;
    @BindView(R.id.progress)
    ProgressBar progressBar;

    private TextView title_action_bar;

    Date currentDate = new Date();

    Integer id_cashier = null;
    Integer id_company = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        handler = new Handler(Looper.getMainLooper());
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null){
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            View viewActionBar = getLayoutInflater().inflate(R.layout.custom_action_bar_profile, null);
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    Gravity.CENTER);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled (false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setCustomView(viewActionBar, params);
            title_action_bar = viewActionBar.findViewById(R.id.txt_title);
        }
        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDialogFragment mBottomSheetDialog = new CustomDialogFragment();
                mBottomSheetDialog.show(getSupportFragmentManager(),"add_photo_dialog_fragment");
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        App.getApi().getInfoCashier(App.BEARER+App.getToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()){
                        String strResponse = response.body().string();
                        JSONObject jsonObject = new JSONObject(strResponse);
                        final String jsonString = jsonObject.getString("data");
                        GsonBuilder builder = new GsonBuilder();
                        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
                        Gson gson = builder.create();
                        final InfoCashier infoCashier = gson.fromJson(jsonString, InfoCashier.class);
                        Log.d(TAG, infoCashier.getCompany().getInnerColor().getHex());
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (infoCashier.getAvatar()!=null){
                                    String urlAvatar = infoCashier.getAvatar();
                                    Picasso picasso = PicassoUtil.preparePicasso(ProfileActivity.this);
                                    if (picasso!=null){
                                        picasso.load(urlAvatar).into(circleImageView);
                                    }
                                }
                                String fio = infoCashier.getFio();
                                if (fio!=null){
                                    title_action_bar.setText(fio);
                                    txtFio.setText(fio);
                                }
                                String login = infoCashier.getLogin();
                                if (login!=null){
                                    txtLogin.setText(login);
                                }
                                if (infoCashier.getCompany()!=null){
                                    String pointName = infoCashier.getCompany().getInnerName();
                                    String hex = infoCashier.getCompany().getInnerColor().getHex();
                                    if (pointName!=null){
                                        txtCompany.setText(pointName);
                                    }
                                    if (hex!=null){
                                        setBackgroundDrawable((StateListDrawable) circle.getBackground(), hex);
                                    }
                                }
                            }
                        });

                        id_cashier = infoCashier.getId();
                        id_company = infoCashier.getCompany().getId();
                        if (id_cashier!=null&&id_company!=null){
                            loadOperations(new Date(),id_cashier,id_company,true, false);
                        }
                    }else {
                        Log.d(TAG, response.errorBody().string());
                    }
                } catch (Exception e) {
                    Log.d(TAG, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    private String textCount(int count){
        if (count == 1){
            return "скидка";
        }else if (count > 1 && count < 5){
            return "скидки";
        }else {
            return "скидок";
        }
    }



    private void loadOperations(final Date date, int cashier_id, int company_id, final boolean b, final boolean allOperations){
        String strDt = null;
        if (!allOperations){
            SimpleDateFormat simpleDate =  new SimpleDateFormat("yyyy-MM-dd");
            strDt = simpleDate.format(date);
        }
        final String finalStrDt = strDt;
        App.getApi().getOperations(App.BEARER+App.getToken(),cashier_id,
                company_id, strDt).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        String strResponse = response.body().string();
                        Log.d(TAG, strResponse);
                        GsonBuilder builder = new GsonBuilder();
                        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
                        Gson gson = builder.create();
                        final InfoOperations infoOperations = gson.fromJson(strResponse, InfoOperations.class);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                int size = infoOperations.getData().getObject().size();
                                if (size>0){
                                    recyclerView.setVisibility(View.VISIBLE);
                                    view_not_disc.setVisibility(View.GONE);
                                    btn_add_disc.setVisibility(View.GONE);
                                    if (allOperations){
                                        List<RecyclerViewAdapterDivideDate.ListItem> items = new ArrayList<>();
                                        Map<Date, List<OperationItem>> operations = toMap(load(infoOperations));
                                        for (Date date : operations.keySet()) {
                                            RecyclerViewAdapterDivideDate.HeaderItem header = new RecyclerViewAdapterDivideDate.HeaderItem(date);
                                            items.add(header);
                                            for (OperationItem event : operations.get(date)) {
                                                RecyclerViewAdapterDivideDate.BodyItem item = new RecyclerViewAdapterDivideDate.BodyItem(event);
                                                items.add(item);
                                            }
                                        }
                                        recyclerView.setAdapter(new RecyclerViewAdapterDivideDate(items,ProfileActivity.this));
                                    }else {
                                        recyclerView.setAdapter(new RecyclerViewAdapterOperations(infoOperations,ProfileActivity.this));
                                    }
                                }else {
                                    recyclerView.setVisibility(View.GONE);
                                    view_not_disc.setVisibility(View.VISIBLE);
                                    btn_add_disc.setVisibility(View.VISIBLE);
                                    Calendar cal1 = Calendar.getInstance();
                                    Calendar cal2 = Calendar.getInstance();
                                    cal1.setTime(date);
                                    cal2.setTime(new Date());
                                    boolean sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                                            cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
                                    if (sameDay){
                                        txt_not_disc.setText("Сегодня еще не было сделано скидок");
                                    }else {
                                        if (finalStrDt==null){
                                            txt_not_disc.setText("Не было сделано скидок");
                                        }else {
                                            txt_not_disc.setText(finalStrDt +" не было сделано скидок");
                                        }

                                    }
                                }
                                if (b){
                                    txt_today_disc_count.setText("Сегодня: "+size+" "+textCount(size));
                                    if (size>0){
                                        setVisibleBottom(true);
                                        int total_disc = 0;
                                        int today_amount = 0;
                                        int today_disc_amount = 0;
                                        for (InfoOperations.Object o : infoOperations.getData().getObject()){
                                            total_disc += o.getDiscountDetail().getDiscountValue() + o.getDiscountDetail().getDrawPointsValue();
                                            today_amount += o.getPrice();
                                            today_disc_amount += o.getTotalPrice();
                                        }
                                        txt_today_disc.setText("Скидки: "+total_disc+"Р");
                                        txt_today_amount.setText("Сумма: "+today_amount+"Р");
                                        txt_today_disc_amount.setText(today_disc_amount+"Р");
                                    }else {
                                        setVisibleBottom(false);
                                    }
                                }
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    try {
                        String strResponse = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(strResponse);
                        JSONObject errors = jsonObject.getJSONObject("errors");
                        String description = errors.getString("description");
                        Log.d(TAG, description);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }



    @NonNull
    private List<OperationItem> load(InfoOperations infoOperations) {
        List<OperationItem> operations = new ArrayList<>();
        for (InfoOperations.Object object : infoOperations.getData().getObject()){
            try {
                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                input.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date date2 = input.parse(object.getCreatedAt());
                input.setTimeZone(TimeZone.getDefault());
                String formattedDate = input.format(date2);
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                Date date = format.parse(formattedDate);
                String fio = object.getUser().getPersonalInformation().getFirstName()+" "+object.getUser().getPersonalInformation().getLastName();
                String profileImage = object.getUser().getPersonalInformation().getAvatar();
                int i = object.getDiscountDetail().getDiscountValue() + object.getDiscountDetail().getDrawPointsValue();
                String disc = object.getDiscountDetail().getPrice()+"Р Скидка: "+i+"Р";
                String total = object.getDiscountDetail().getTotalPrice()+"Р";
                SimpleDateFormat output = new SimpleDateFormat("HH:mm",Locale.ENGLISH);
                Date oneWayTripDate = input.parse(formattedDate);
                String time = output.format(oneWayTripDate);
                operations.add(new OperationItem(fio,date,profileImage,disc,total,time));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return operations;
    }

    @NonNull
    private Map<Date, List<OperationItem>> toMap(@NonNull List<OperationItem> operations) {
        Map<Date, List<OperationItem>> map = new TreeMap<>();
        for (OperationItem event : operations) {
            List<OperationItem> value = map.get(event.getDate());
            if (value == null) {
                value = new ArrayList<>();
                map.put(event.getDate(), value);
            }
            value.add(event);
        }
        return map;
    }

    private void setVisibleBottom(boolean b){
        if (b){
            txt_today_disc.setVisibility(View.VISIBLE);
            txt_today_amount.setVisibility(View.VISIBLE);
            txt_today_disc_amount.setVisibility(View.VISIBLE);
        }else{
            txt_today_disc.setVisibility(View.INVISIBLE);
            txt_today_amount.setVisibility(View.INVISIBLE);
            txt_today_disc_amount.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onActionClickGallery() {
        if (Build.VERSION.SDK_INT >= 23){
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {


                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                }
            }else{
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        }else {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            Log.d("TAG", "onActionClickGallery");
        }

    }

    @Override
    public void onActionClickCamera() {
        Log.d("TAG", String.valueOf(Build.VERSION.SDK_INT));
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        MY_CAMERA_PERMISSION_CODE);
            } else {
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        } else {
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    }

    private void showDatePickerDialog(View v, Date start) {
        DialogFragment newFragment = new DatePickerFragment(this, v, start);
        newFragment.show(getFragmentManager(), "datePicker");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new
                        Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }

        }else if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE){
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            } else {
                Toast.makeText(this, "read external storage permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            circleImageView.setImageBitmap(photo);
            File file = createAvatarFile(photo);
            if (file!=null){
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);
                    App.getApi().uploadAvatar(App.BEARER+App.getToken(),body).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()){
                                Log.d(TAG, "onResponse isSuccessful");
                            }else {
                                Log.d(TAG, "onResponse not isSuccessful");
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Log.d(TAG, "onFailure uploadAvatar " +t.getMessage());
                        }
                    });
            }
        }else if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK && data != null){
            Uri pickedImage = data.getData();
            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
            circleImageView.setImageBitmap(bitmap);
            File file = createAvatarFile(bitmap);
            if (file!=null){
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);
                App.getApi().uploadAvatar(App.BEARER+App.getToken(),body).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            Log.d(TAG, "onResponse isSuccessful");
                        }else {
                            Log.d(TAG, "onResponse not isSuccessful");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d(TAG, "onFailure uploadAvatar " +t.getMessage());
                    }
                });
            }
            cursor.close();
        }
    }

    private File createAvatarFile(Bitmap bitmap){
        ///create a file to write bitmap data
        try {
            File f = new File(getCacheDir(), "avatar");
            //Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();
            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return f;
        } catch (IOException e) {
            e.printStackTrace();
        }
       return null;
    }

    public void onClickActionNext(View view) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public void onClickActionExit(View view) {
        App.setToken(null);
        App.setQrCode(null);
        new Delete().from(Discount.class).execute();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
        overridePendingTransition(R.anim.push_in_up, R.anim.push_out_up);
    }


    private void setBackgroundDrawable(StateListDrawable stateListDrawable, String color){
        DrawableContainer.DrawableContainerState drawableContainerState = (DrawableContainer.DrawableContainerState) stateListDrawable.getConstantState();
        Drawable[] children = new Drawable[0];
        if (drawableContainerState != null) {
            children = drawableContainerState.getChildren();
            GradientDrawable drawable = (GradientDrawable) children[0];
            drawable.setColor(Color.parseColor(color));
        }
    }

    public void onClickActionAddDiscount(View view) {
        startActivityForResult(new Intent(getApplicationContext(), AddDiscountActivity.class), REQUEST_CODE_ADD_DISCOUNT);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    public void onDateChanged(View v, int year, int month, int day) {
        Calendar dateShip = Calendar.getInstance();
        dateShip.set(Calendar.YEAR, year);
        dateShip.set(Calendar.MONTH, month);
        dateShip.set(Calendar.DAY_OF_MONTH, day);
        currentDate = dateShip.getTime();
        if (id_cashier!=null&&id_company!=null){
            loadOperations(currentDate,id_cashier,id_company,false, false);
        }
    }

    public void onClickActionCalendar(View view) {
        showDatePickerDialog(view, currentDate);
    }

    public void onClickActionDiscAllTime(View view) {
        loadOperations(new Date(),id_cashier,id_company,false, true);
    }
}
