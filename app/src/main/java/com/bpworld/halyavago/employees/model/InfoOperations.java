package com.bpworld.halyavago.employees.model;

/**
 * Created by GrinfeldRA
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InfoOperations {

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class InnerColor {

        @SerializedName("hex")
        @Expose
        private String hex;

        public String getHex() {
            return hex;
        }

        public void setHex(String hex) {
            this.hex = hex;
        }

    }


    public class Object {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("company_id")
        @Expose
        private Integer companyId;
        @SerializedName("cashier_id")
        @Expose
        private Integer cashierId;
        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("discount_price")
        @Expose
        private Integer discountPrice;
        @SerializedName("total_price")
        @Expose
        private Integer totalPrice;
        @SerializedName("discount_detail")
        @Expose
        private DiscountDetail discountDetail;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("user")
        @Expose
        private User user;
        @SerializedName("company")
        @Expose
        private Company company;
        @SerializedName("cashier")
        @Expose
        private Cashier cashier;
        @SerializedName("operation_reward")
        @Expose
        private OperationReward operationReward;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getCompanyId() {
            return companyId;
        }

        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        public Integer getCashierId() {
            return cashierId;
        }

        public void setCashierId(Integer cashierId) {
            this.cashierId = cashierId;
        }

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public Integer getDiscountPrice() {
            return discountPrice;
        }

        public void setDiscountPrice(Integer discountPrice) {
            this.discountPrice = discountPrice;
        }

        public Integer getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(Integer totalPrice) {
            this.totalPrice = totalPrice;
        }

        public DiscountDetail getDiscountDetail() {
            return discountDetail;
        }

        public void setDiscountDetail(DiscountDetail discountDetail) {
            this.discountDetail = discountDetail;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public Company getCompany() {
            return company;
        }

        public void setCompany(Company company) {
            this.company = company;
        }

        public Cashier getCashier() {
            return cashier;
        }

        public void setCashier(Cashier cashier) {
            this.cashier = cashier;
        }

        public OperationReward getOperationReward() {
            return operationReward;
        }

        public void setOperationReward(OperationReward operationReward) {
            this.operationReward = operationReward;
        }

    }

    public class PersonalInformation {

        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("avatar")
        @Expose
        private String avatar;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

    }


    public class User {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("personal_information")
        @Expose
        private PersonalInformation personalInformation;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public PersonalInformation getPersonalInformation() {
            return personalInformation;
        }

        public void setPersonalInformation(PersonalInformation personalInformation) {
            this.personalInformation = personalInformation;
        }

    }

    public class Skidka {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("original_price")
        @Expose
        private Integer originalPrice;
        @SerializedName("discount_value")
        @Expose
        private Integer discountValue;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getOriginalPrice() {
            return originalPrice;
        }

        public void setOriginalPrice(Integer originalPrice) {
            this.originalPrice = originalPrice;
        }

        public Integer getDiscountValue() {
            return discountValue;
        }

        public void setDiscountValue(Integer discountValue) {
            this.discountValue = discountValue;
        }

    }

    public class OperationReward {

        @SerializedName("value")
        @Expose
        private Integer value;

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

    }

    public class Cashier {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("fio")
        @Expose
        private String fio;
        @SerializedName("avatar")
        @Expose
        private String avatar;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFio() {
            return fio;
        }

        public void setFio(String fio) {
            this.fio = fio;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

    }

    public class Company {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("inner_name")
        @Expose
        private String innerName;
        @SerializedName("inner_color")
        @Expose
        private InnerColor innerColor;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getInnerName() {
            return innerName;
        }

        public void setInnerName(String innerName) {
            this.innerName = innerName;
        }

        public InnerColor getInnerColor() {
            return innerColor;
        }

        public void setInnerColor(InnerColor innerColor) {
            this.innerColor = innerColor;
        }

    }

    public class Data {

        @SerializedName("finish")
        @Expose
        private Boolean finish;
        @SerializedName("object")
        @Expose
        private List<Object> object = null;

        public Boolean getFinish() {
            return finish;
        }

        public void setFinish(Boolean finish) {
            this.finish = finish;
        }

        public List<Object> getObject() {
            return object;
        }

        public void setObject(List<Object> object) {
            this.object = object;
        }

    }

    public class DiscountDetail {

        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("discount_value")
        @Expose
        private Integer discountValue;
        @SerializedName("discount_percent")
        @Expose
        private Integer discountPercent;
        @SerializedName("discount_price")
        @Expose
        private Integer discountPrice;
        @SerializedName("draw_points_value")
        @Expose
        private Integer drawPointsValue;
        @SerializedName("draw_points_percent")
        @Expose
        private Integer drawPointsPercent;
        @SerializedName("total_price")
        @Expose
        private Integer totalPrice;
        @SerializedName("discounts")
        @Expose
        private Discounts discounts;

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public Integer getDiscountValue() {
            return discountValue;
        }

        public void setDiscountValue(Integer discountValue) {
            this.discountValue = discountValue;
        }

        public Integer getDiscountPercent() {
            return discountPercent;
        }

        public void setDiscountPercent(Integer discountPercent) {
            this.discountPercent = discountPercent;
        }

        public Integer getDiscountPrice() {
            return discountPrice;
        }

        public void setDiscountPrice(Integer discountPrice) {
            this.discountPrice = discountPrice;
        }

        public Integer getDrawPointsValue() {
            return drawPointsValue;
        }

        public void setDrawPointsValue(Integer drawPointsValue) {
            this.drawPointsValue = drawPointsValue;
        }

        public Integer getDrawPointsPercent() {
            return drawPointsPercent;
        }

        public void setDrawPointsPercent(Integer drawPointsPercent) {
            this.drawPointsPercent = drawPointsPercent;
        }

        public Integer getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(Integer totalPrice) {
            this.totalPrice = totalPrice;
        }

        public Discounts getDiscounts() {
            return discounts;
        }

        public void setDiscounts(Discounts discounts) {
            this.discounts = discounts;
        }

    }

    public class Discounts {

        @SerializedName("skidka")
        @Expose
        private Skidka skidka;

        public Skidka getSkidka() {
            return skidka;
        }

        public void setSkidka(Skidka skidka) {
            this.skidka = skidka;
        }

    }

}
