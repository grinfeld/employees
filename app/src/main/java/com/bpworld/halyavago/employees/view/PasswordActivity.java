package com.bpworld.halyavago.employees.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.bpworld.halyavago.employees.App;
import com.bpworld.halyavago.employees.PicassoUtil;
import com.bpworld.halyavago.employees.R;
import com.bpworld.halyavago.employees.model.OauthCashier;
import com.bpworld.halyavago.employees.model.OauthToken;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by GrinfeldRA
 */

public class PasswordActivity extends AppCompatActivity {

    private static final String TAG = "#PasswordActivity";
    private String clientSecret = "k6WTx6IuUT9OAe3YfZAHWn2TRJyio27CuIiL0Cze";
    private String strResponse;
    private String login;
    private String provider = "cashiers";

    @BindView(R.id.input_password)
    EditText passwordText;
    @BindView(R.id.profile_image)
    CircleImageView profile_image;
    @BindView(R.id.txt_fio)
    TextView txtFio;
    @BindView(R.id.txt_company)
    TextView txtCompany;

    private Handler handler;
    private TextView title_action_bar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        ButterKnife.bind(this);
        strResponse = getIntent().getStringExtra(LoginActivity.STR_RESPONSE);
        login = getIntent().getStringExtra(LoginActivity.LOGIN);
        handler = new Handler(Looper.getMainLooper());
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            View viewActionBar = getLayoutInflater().inflate(R.layout.custom_action_bar, null);
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    Gravity.CENTER);
            actionBar.setCustomView(viewActionBar, params);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            title_action_bar = viewActionBar.findViewById(R.id.txt_title);
        }
        passwordText.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(this, R.drawable.ic_password), null);
        try {
            JSONObject jsonObject = new JSONObject(strResponse);
            String jsonString = jsonObject.getString("data");
            Gson gson = new Gson();
            final OauthCashier cashier = gson.fromJson(jsonString, OauthCashier.class);
            title_action_bar.setText(cashier.getFio());
            txtFio.setText(cashier.getFio());
            txtCompany.setText(cashier.getCompany().getInnerName());
            Picasso picasso = PicassoUtil.preparePicasso(PasswordActivity.this);
            if (picasso != null) {
                picasso.load(cashier.getAvatar()).into(profile_image);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        return true;
    }


    public void onClickActionLogin(View view) {
        String password = passwordText.getText().toString();
        if (!password.isEmpty()) {
            App.getApi().getOauthToken("2", clientSecret, login, passwordText.getText().toString(), provider).enqueue(new Callback<OauthToken>() {
                @Override
                public void onResponse(Call<OauthToken> call, final Response<OauthToken> response) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (response.isSuccessful()) {
                                String token = response.body().getData().getAccessToken();
                                Log.d(TAG, "onResponse " + token);
                                App.setToken(token);
                                startActivity(new Intent(PasswordActivity.this, ProfileActivity.class));
                                finish();
                                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                            } else {
//                                startActivity(new Intent(PasswordActivity.this, NoAccessActivity.class));
//                                finish();
//                                overridePendingTransition(R.anim.push_in_up, R.anim.push_out_up);
                                try {
                                    String strResponse = response.errorBody().string();
                                    JSONObject jsonObject = new JSONObject(strResponse);
                                    JSONObject data = jsonObject.getJSONObject("errors");
                                    String description = data.getString("description");
                                    Toast.makeText(PasswordActivity.this, description, Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }

                @Override
                public void onFailure(Call<OauthToken> call, Throwable t) {
                    Log.d(TAG, "onFailure2 " + t.getMessage());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(PasswordActivity.this, NoAccessActivity.class));
                            finish();
                            overridePendingTransition(R.anim.push_in_up, R.anim.push_out_up);
                        }
                    });
                }
            });
        } else {
            Toast.makeText(this, "Введите пароль", Toast.LENGTH_SHORT).show();
        }
    }
}
