package com.bpworld.halyavago.employees.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.blikoon.qrcodescanner.QrCodeActivity;
import com.bpworld.halyavago.employees.App;
import com.bpworld.halyavago.employees.R;
import com.bpworld.halyavago.employees.adapters.RecyclerViewAdapterDiscount;
import com.bpworld.halyavago.employees.model.Discount;

import org.json.JSONException;
import org.json.JSONObject;

import java.awt.font.TextAttribute;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements RecyclerViewAdapterDiscount.IonDelete {

    private static final String TAG = "#MainActivity";

    @BindView(R.id.input_qr_code)
    EditText input_qr_code;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.action_add_discount)
    TextView action_add_discount;
    ImageButton img_btn_add;

    private Handler handler;
    public static final String CALC_CHECK_INFO = "calc_check_info";
    public static final String QR = "qr";

    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    public static final int REQUEST_CODE_ADD_DISCOUNT = 102;
    List<Discount> execute;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        handler = new Handler(Looper.getMainLooper());
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            View viewActionBar = getLayoutInflater().inflate(R.layout.custom_action_bar_discount, null);
            img_btn_add = viewActionBar.findViewById(R.id.img_btn_add);
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    Gravity.CENTER);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setCustomView(viewActionBar, params);
        }
        if (App.getQrCode() != null) {
            input_qr_code.setText(App.getQrCode());
        }
        input_qr_code.addTextChangedListener(new TextWatcher() {
            int prevL = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                prevL = input_qr_code.getText().toString().length();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                App.setQrCode(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                int length = s.length();
                if ((prevL < length) && (length == 2 || length == 5 || length == 8 ||
                        length == 11 || length == 14 || length == 17 || length == 21)) {
                    s.append("-");
                }
            }
        });
        input_qr_code.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(this, R.drawable.ic_qr_code_1), null);
        input_qr_code.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (input_qr_code.getRight()
                            - input_qr_code.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        onActionClickQR();
                    }
                }
                return false;
            }
        });
        execute = new Select().from(Discount.class).execute();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new RecyclerViewAdapterDiscount(execute, this, this));

        initBtn(execute.size());


        action_add_discount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch ((String) v.getTag()) {
                    case "NEXT":
                        final String qr = input_qr_code.getText().toString().trim().replaceAll("-", "");
                        Log.d(TAG, qr);
                        JSONObject jsonObject = new JSONObject();
                        List<Discount> execute = new Select().from(Discount.class).execute();
                        for (Discount discount : execute) {
                            try {
                                jsonObject.put(String.valueOf(discount.getIdDiscount()), discount.getCost());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        if (qr.isEmpty()) {
                            Toast.makeText(MainActivity.this, "Введите код пользователя", Toast.LENGTH_LONG).show();
                            return;
                        }
                        App.getApi().calcCheck(App.BEARER + App.getToken(), qr, jsonObject.toString()).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    try {
                                        String strResponse = response.body().string();
                                        JSONObject jsonObject = new JSONObject(strResponse);
                                        final String jsonString = jsonObject.getString("data");
                                        Log.d("onClickActionNext: ", jsonString);
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                startActivity(new Intent(MainActivity.this, CalculationActivity.class).putExtra(CALC_CHECK_INFO, jsonString).putExtra(QR, qr));
                                                finish();
                                                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                String strResponse = response.errorBody().string();
                                                JSONObject jsonObject = new JSONObject(strResponse);
                                                JSONObject errors = jsonObject.getJSONObject("errors");
                                                String description = errors.getString("description");
                                                String code = errors.getString("code");
                                                Log.d(TAG, code + "  " + description);
                                                if (code.equals("15")) {
                                                    startActivity(new Intent(MainActivity.this, NoAccessActivity.class));
                                                    finish();
                                                    overridePendingTransition(R.anim.push_in_up, R.anim.push_out_up);
                                                } else {
                                                    Toast.makeText(MainActivity.this, "Неверный код пользователя", Toast.LENGTH_SHORT).show();
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.d("onClickActionNext: ", "Throwable");
                            }
                        });
                        break;
                    case "ADD":
                        startActivityForResult(new Intent(getApplicationContext(), AddDiscountActivity.class), REQUEST_CODE_ADD_DISCOUNT);
                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                        break;
                }

            }
        });

    }

    public void onActionClickQR() {

        Log.d("TAG", String.valueOf(Build.VERSION.SDK_INT));
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        MY_CAMERA_PERMISSION_CODE);
            } else {
                Intent i = new Intent(MainActivity.this, QrCodeActivity.class);
                startActivityForResult(i, REQUEST_CODE_QR_SCAN);
            }
        } else {
            Intent i = new Intent(MainActivity.this, QrCodeActivity.class);
            startActivityForResult(i, REQUEST_CODE_QR_SCAN);
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                Intent i = new Intent(MainActivity.this, QrCodeActivity.class);
                startActivityForResult(i, REQUEST_CODE_QR_SCAN);
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void initBtn(int sizeData) {
        if (sizeData > 0) {
            action_add_discount.setTag("NEXT");
            action_add_discount.setText("Пробить скидки");
            setBackgroundDrawable(this, R.drawable.btn_selector_green, action_add_discount);
            img_btn_add.setVisibility(View.VISIBLE);
        } else {
            action_add_discount.setTag("ADD");
            action_add_discount.setText("Добавить скидку");
            setBackgroundDrawable(this, R.drawable.btn_selector_orange, action_add_discount);
            img_btn_add.setVisibility(View.GONE);
        }
    }

    private void setBackgroundDrawable(Context context, int id, View view) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(ContextCompat.getDrawable(context, id));
        } else {
            view.setBackground(ContextCompat.getDrawable(context, id));
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_ADD_DISCOUNT:
//                if (data==null)
//                    return;
//                discount = (ArrayList<DiscountType.Discount>) data.getBundleExtra("key").getSerializable("key");
//                recyclerView.setAdapter(new RecyclerViewAdapterDiscount(discount, this));
//                Log.d("onActivityResult: ", discount.get(0).getTitle());
                List<Discount> execute = new Select().from(Discount.class).execute();
                recyclerView.setAdapter(new RecyclerViewAdapterDiscount(execute, this, this));
                initBtn(execute.size());
                break;
            case REQUEST_CODE_QR_SCAN:
                if (resultCode != Activity.RESULT_OK) {
                    if (data == null)
                        return;
                    //Getting the passed result
                    String result = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
                    if (result != null) {
                        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                        alertDialog.setTitle("Scan Error");
                        alertDialog.setMessage("QR Code could not be scanned");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                    return;

                } else {
                    if (data == null)
                        return;
                    //Getting the passed result
                    String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
                    input_qr_code.setText(result);
                }
                break;
        }

    }


    public void onClickActionProfile(View view) {
        startActivity(new Intent(this, ProfileActivity.class));
        finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }


    public void onClickActionAdd(View view) {
        startActivityForResult(new Intent(getApplicationContext(), AddDiscountActivity.class), REQUEST_CODE_ADD_DISCOUNT);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    public void onActionDelete(int sizeData) {
        initBtn(sizeData);
    }
}
