package com.bpworld.halyavago.employees.model;

/**
 * Created by GrinfeldRA
 */

public class OperationSummary {
    private String title;
    private String amount;
    private String discount;


    public OperationSummary(String title, String amount, String discount) {
        this.title = title;
        this.amount = amount;
        this.discount = discount;
    }


    public String getTitle() {
        return title;
    }

    public String getAmount() {
        return amount;
    }

    public String getDiscount() {
        return discount;
    }

}
