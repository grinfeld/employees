package com.bpworld.halyavago.employees.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.activeandroid.query.Delete;
import com.bpworld.halyavago.employees.App;
import com.bpworld.halyavago.employees.R;
import com.bpworld.halyavago.employees.model.Discount;

import butterknife.ButterKnife;

/**
 * Created by GrinfeldRA
 */

public class NoAccessActivity extends AppCompatActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_access);
        ButterKnife.bind(this);
        new Delete().from(Discount.class).execute();
        App.setQrCode(null);
        App.setToken(null);
    }

    public void onClickActionRefresh(View view) {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
        overridePendingTransition(R.anim.push_in_up, R.anim.push_out_up);
    }
}
