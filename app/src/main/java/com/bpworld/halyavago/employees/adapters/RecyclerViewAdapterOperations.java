package com.bpworld.halyavago.employees.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bpworld.halyavago.employees.PicassoUtil;
import com.bpworld.halyavago.employees.R;
import com.bpworld.halyavago.employees.model.InfoOperations;
import com.bpworld.halyavago.employees.view.ProfileActivity;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by GrinfeldRA
 */

public class RecyclerViewAdapterOperations extends RecyclerView.Adapter<RecyclerViewAdapterOperations.ViewHolder>{


    private InfoOperations data;
    private LayoutInflater mInflater;
    private Activity activity;


    public RecyclerViewAdapterOperations(InfoOperations data, Activity context) {
        this.data = data;
        this.mInflater = LayoutInflater.from(context);
        activity = context;
    }

    @Override
    public RecyclerViewAdapterOperations.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = mInflater.inflate(R.layout.recycler_row_discount, parent, false);
        return new RecyclerViewAdapterOperations.ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapterOperations.ViewHolder holder, final int position) {
        holder.txt_name.setText(data.getData().getObject().get(position).getUser().getPersonalInformation().getFirstName()+ " "+
                data.getData().getObject().get(position).getUser().getPersonalInformation().getLastName());
        String date = data.getData().getObject().get(position).getCreatedAt();
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        input.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat output = new SimpleDateFormat("HH:mm",Locale.ENGLISH);
        try {
            Date date2 = input.parse(date);
            input.setTimeZone(TimeZone.getDefault());
            String formattedDate = input.format(date2);

            Date oneWayTripDate = input.parse(formattedDate);
            holder.txt_time.setText(output.format(oneWayTripDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Picasso picasso = PicassoUtil.preparePicasso(activity);
        if (picasso!=null){
            picasso.load(data.getData().getObject().get(position).getUser().getPersonalInformation().getAvatar()).into(holder.profile_image);
        }
        //1400Р Скидка: 250Р"
        int i = data.getData().getObject().get(position).getDiscountDetail().getDiscountValue() + data.getData().getObject().get(position).getDiscountDetail().getDrawPointsValue();
        holder.txt_disc.setText(data.getData().getObject().get(position).getDiscountDetail().getPrice()+"Р Скидка: "+i+"Р");
        holder.txt_total.setText(data.getData().getObject().get(position).getDiscountDetail().getTotalPrice()+"Р");
    }

    @Override
    public int getItemCount() {
        return data.getData().getObject().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_name, txt_time,txt_disc, txt_total;
        CircleImageView profile_image;


        public ViewHolder(View itemView) {
            super(itemView);
            txt_name = itemView.findViewById(R.id.txt_name);
            txt_time = itemView.findViewById(R.id.txt_time);
            profile_image = itemView.findViewById(R.id.profile_image);
            txt_disc = itemView.findViewById(R.id.txt_disc);
            txt_total = itemView.findViewById(R.id.txt_total);
        }
    }
}
