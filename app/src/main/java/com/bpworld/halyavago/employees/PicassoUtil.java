package com.bpworld.halyavago.employees;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;

/**
 * Created by GrinfeldRA
 */

public class PicassoUtil {

    private static String TAG = "#PicassoUtil";

    public static Picasso preparePicasso(Context context){
        try {
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, null, null);
            SSLSocketFactory noSSLv3Factory;
            if (Build.VERSION.SDK_INT<=Build.VERSION_CODES.KITKAT) {
                noSSLv3Factory = new TLSSocketFactory();
            } else {
                noSSLv3Factory = sslContext.getSocketFactory();
            }
            OkHttpClient.Builder okb=new OkHttpClient.Builder()
                    .sslSocketFactory(noSSLv3Factory, provideX509TrustManager());
            OkHttpClient ok=okb.build();
            return new Picasso.Builder(context)
                    .downloader(new OkHttp3Downloader(ok))
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static X509TrustManager provideX509TrustManager() {
        try {
            TrustManagerFactory factory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            factory.init((KeyStore) null);
            TrustManager[] trustManagers = factory.getTrustManagers();
            return (X509TrustManager) trustManagers[0];
        }
        catch (NoSuchAlgorithmException exception) {
            Log.e(TAG, "not trust manager available", exception);
        }
        catch (KeyStoreException exception) {
            Log.e(TAG, "not trust manager available", exception);
        }

        return null;
    }
}
