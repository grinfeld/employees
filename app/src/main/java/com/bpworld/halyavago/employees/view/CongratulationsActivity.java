package com.bpworld.halyavago.employees.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.activeandroid.query.Delete;
import com.bpworld.halyavago.employees.App;
import com.bpworld.halyavago.employees.R;
import com.bpworld.halyavago.employees.model.Discount;


/**
 * Created by GrinfeldRA
 */

public class CongratulationsActivity extends AppCompatActivity{


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Delete().from(Discount.class).execute();
        App.setQrCode(null);
        setContentView(R.layout.activity_congratulations);
    }

    public void onClickActionRefresh(View view) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
        overridePendingTransition(R.anim.push_in_up, R.anim.push_out_up);
    }

}
