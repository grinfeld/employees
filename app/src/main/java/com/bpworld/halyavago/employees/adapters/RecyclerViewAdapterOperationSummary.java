package com.bpworld.halyavago.employees.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bpworld.halyavago.employees.R;
import com.bpworld.halyavago.employees.model.OperationSummary;

import java.util.List;

/**
 * Created by GrinfeldRA
 */

public class RecyclerViewAdapterOperationSummary extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<ListItem> items;
    private Activity context;
    private LayoutInflater mInflater;

    public RecyclerViewAdapterOperationSummary(List<ListItem> items, Activity context) {
        this.items = items;
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType){
            case ListItem.TYPE_DISCOUNT:{
                View itemView = mInflater.inflate(R.layout.recyclerview_row_operations_discount, parent, false);
                return new DiscountViewHolder(itemView);
            }
            case ListItem.TYPE_SUMMARY:{
                View itemView = mInflater.inflate(R.layout.recyclerview_row_operations_summary, parent, false);
                return new SummaryViewHolder(itemView);
            }
            default:
                throw new IllegalStateException("unsupported item type");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case ListItem.TYPE_DISCOUNT: {
                DiscountItem discountItem = (DiscountItem) items.get(position);
                DiscountViewHolder viewHolder = (DiscountViewHolder) holder;
                viewHolder.txt_discount.setText(discountItem.getOperationItem().getDiscount());
                viewHolder.txt_amount.setText(discountItem.getOperationItem().getAmount());
                break;
            }
            case ListItem.TYPE_SUMMARY: {
                SummaryItem summaryItem = (SummaryItem) items.get(position);
                SummaryViewHolder viewHolder = (SummaryViewHolder) holder;
                String styledText = summaryItem.getPaidByPoints()+"<font color='#ffd35a'> ( "+summaryItem.getDraw_points_percent()+"% )</font>";
                viewHolder.txt_discount.setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);
                viewHolder.txt_amount.setText(summaryItem.getTotalDiscount());
                viewHolder.txt_price.setText(summaryItem.getPrimatyAmount());
                String styledText2 = summaryItem.getDiscount()+"<font color='#ffd35a'> ( "+summaryItem.getDiscount_percent()+"%)</font>";
                viewHolder.txt_disc.setText(Html.fromHtml(styledText2), TextView.BufferType.SPANNABLE);
                int discount = 0;
                int totalDisc = 0;
                try {
                    float primary = Float.parseFloat(summaryItem.getPrimatyAmount().substring(0,summaryItem.getPrimatyAmount().length()-1));
                    float total = Float.parseFloat(summaryItem.getTotalSum().substring(0,summaryItem.getTotalSum().length()-1));
                    totalDisc  = Math.round(primary - total);
                    float onePercent = primary / 100f;
                    float v = total / onePercent;
                    discount = 100 - Math.round(v);
                }catch (Exception e){
                    e.printStackTrace();
                }

                String styledText3 = summaryItem.getTotalSum()+"<br/>Скидка: "+totalDisc+"<font color='#ffd35a'> ( "+discount+"% )</font>";
                viewHolder.txt_total.setText(Html.fromHtml(styledText3), TextView.BufferType.SPANNABLE);
                break;
            }
            default:
                throw new IllegalStateException("unsupported item type");
        }
    }



    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    public static abstract class ListItem {

        static final int TYPE_DISCOUNT = 0;
        static final int TYPE_SUMMARY = 1;
        abstract public int getType();
    }

    public static class SummaryItem extends ListItem {

        @NonNull
        private String discount, primatyAmount, totalDiscount, paidByPoints, totalSum, draw_points_percent, discount_percent;


        public SummaryItem(@NonNull String primaryAmount, @NonNull String discount,
                           @NonNull String totalDiscount, @NonNull String paidByPoints,
                           @NonNull String totalSum, @NonNull String draw_points_percent,
                           @NonNull String discount_percent) {
            this.discount = discount;
            this.primatyAmount = primaryAmount;
            this.totalDiscount = totalDiscount;
            this.paidByPoints = paidByPoints;
            this.totalSum = totalSum;
            this.draw_points_percent = draw_points_percent;
            this.discount_percent = discount_percent;
        }


        @NonNull
        public String getDraw_points_percent() {
            return draw_points_percent;
        }

        @NonNull
        public String getDiscount_percent() {
            return discount_percent;
        }

        @NonNull
        public String getDiscount() {
            return discount;
        }

        @NonNull
        public String getPrimatyAmount() {
            return primatyAmount;
        }

        @NonNull
        public String getTotalDiscount() {
            return totalDiscount;
        }

        @NonNull
        public String getPaidByPoints() {
            return paidByPoints;
        }

        @Override
        public int getType() {
            return TYPE_SUMMARY;
        }

        @NonNull
        public String getTotalSum() {
            return totalSum;
        }

        public void setTotalSum(@NonNull String totalSum) {
            this.totalSum = totalSum;
        }
    }

    public static class DiscountItem extends ListItem {

        @NonNull
        private OperationSummary operationItem;

        public DiscountItem(@NonNull OperationSummary operationItem) {
            this.operationItem = operationItem;
        }

        @NonNull
        public OperationSummary getOperationItem() {
            return operationItem;
        }

        @Override
        public int getType() {
            return TYPE_DISCOUNT;
        }

    }

    private class DiscountViewHolder extends RecyclerView.ViewHolder {

        TextView txt_amount, txt_discount;

        public DiscountViewHolder(View itemView) {
            super(itemView);
            txt_amount = itemView.findViewById(R.id.txt_amount);
            txt_discount = itemView.findViewById(R.id.txt_discount);
        }
    }

    private class SummaryViewHolder extends RecyclerView.ViewHolder {

        TextView txt_amount, txt_discount, txt_total, txt_disc, txt_price;

        public SummaryViewHolder(View itemView) {
            super(itemView);
            txt_amount = itemView.findViewById(R.id.txt_amount);
            txt_discount = itemView.findViewById(R.id.txt_discount);
            txt_total = itemView.findViewById(R.id.txt_total);
            txt_disc = itemView.findViewById(R.id.txt_disc);
            txt_price = itemView.findViewById(R.id.txt_price);
        }
    }
}
