package com.bpworld.halyavago.employees.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.activeandroid.query.Delete;
import com.bpworld.halyavago.employees.R;
import com.bpworld.halyavago.employees.model.Discount;

import java.util.List;

/**
 * Created by GrinfeldRA
 */

public class RecyclerViewAdapterDiscount extends RecyclerView.Adapter<RecyclerViewAdapterDiscount.ViewHolder>{


    private List<Discount> data;
    private LayoutInflater mInflater;
    private IonDelete ionDelete;

    public interface IonDelete{
        void onActionDelete(int sizeData);
    }

    public RecyclerViewAdapterDiscount(List<Discount> data, Activity context, IonDelete ionDelete) {
        this.data = data;
        this.mInflater = LayoutInflater.from(context);
        this.ionDelete = ionDelete;
    }

    @Override
    public RecyclerViewAdapterDiscount.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = mInflater.inflate(R.layout.recyclerview_row_discount, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapterDiscount.ViewHolder holder, final int position) {

        int cost = data.get(position).getCost().intValue();
        float discount = data.get(position).getValue();
        float v = cost / 100f;
        float v1 =  v * discount;

        holder.txt_title.setText(data.get(position).getTitle());
        holder.txt_amount.setText(String.valueOf(cost)+"Р");
        String styledText = Math.round(v1)+"Р<font color='#ffd35a'> ( "+Math.round(discount)+"% )</font>";
        holder.txt_discount.setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);
        holder.action_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Delete().from(Discount.class).where("_id = ?", data.get(position).getId()).execute();
                data.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, getItemCount());
                ionDelete.onActionDelete(data.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_title, txt_discount, txt_amount;
        ImageView action_delete;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_title = itemView.findViewById(R.id.txt_title);
            txt_discount = itemView.findViewById(R.id.txt_discount);
            txt_amount = itemView.findViewById(R.id.txt_amount);
            action_delete = itemView.findViewById(R.id.action_delete);
        }
    }
}
