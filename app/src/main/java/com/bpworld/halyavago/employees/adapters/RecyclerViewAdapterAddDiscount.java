package com.bpworld.halyavago.employees.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;


import com.bpworld.halyavago.employees.R;
import com.bpworld.halyavago.employees.model.Discount;

import net.igenius.customcheckbox.CustomCheckBox;

import java.util.List;

/**
 * Created by GrinfeldRA
 */

public class RecyclerViewAdapterAddDiscount extends RecyclerView.Adapter<RecyclerViewAdapterAddDiscount.ViewHolder>{

    private static final String TAG = "#RecyclerAddDiscount";
    private List<Discount> data;
    private LayoutInflater mInflater;
    private Activity activity;
    private OnItemCheckListener listener;

    public interface OnItemCheckListener {
        void onItemCheck(Discount item);
        void onItemUnCheck(Discount item);
    }

    public RecyclerViewAdapterAddDiscount(List<Discount> data, Activity context, OnItemCheckListener listener) {
        this.data = data;
        this.mInflater = LayoutInflater.from(context);
        activity = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = mInflater.inflate(R.layout.recyclerview_row_add_discount, parent, false);
        return new RecyclerViewAdapterAddDiscount.ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.title.setText(data.get(position).getTitle());
        final CustomCheckBox checkBox = holder.itemView.findViewById(R.id.checkbox);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(activity);
                if (checkBox.isChecked()){
                    checkBox.setChecked(false);
                }else {
                    checkBox.setChecked(true);
                }
            }
        });
        checkBox.setOnCheckedChangeListener(new CustomCheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomCheckBox checkBox, boolean isChecked) {
                if (isChecked){
                    String disc = holder.edit_discount.getText().toString();
                    if (!disc.isEmpty()){
                        data.get(position).setCost(Float.parseFloat(disc));
                    }
                    listener.onItemCheck(data.get(position));
                }else {
                    listener.onItemUnCheck(data.get(position));
                }
            }
        });
        holder.edit_discount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0){
                    checkBox.setChecked(true);
                }else {
                    checkBox.setChecked(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        EditText edit_discount;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.txt_title);
            edit_discount = itemView.findViewById(R.id.edit_discount);
        }
    }
}
