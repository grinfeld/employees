package com.bpworld.halyavago.employees.model;

import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Expose;

/**
 * Created by GrinfeldRA
 */


public class InfoCashier {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("fio")
    @Expose
    private String fio;
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("company")
    @Expose
    private Company company;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }


    public class Company {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("inner_name")
        @Expose
        private String innerName;
        @SerializedName("inner_color_id")
        @Expose
        private Integer innerColorId;
        @SerializedName("inner_color")
        @Expose
        private InnerColor innerColor;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getInnerName() {
            return innerName;
        }

        public void setInnerName(String innerName) {
            this.innerName = innerName;
        }

        public Integer getInnerColorId() {
            return innerColorId;
        }

        public void setInnerColorId(Integer innerColorId) {
            this.innerColorId = innerColorId;
        }

        public InnerColor getInnerColor() {
            return innerColor;
        }

        public void setInnerColor(InnerColor innerColor) {
            this.innerColor = innerColor;
        }

    }

    public class InnerColor {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("hex")
        @Expose
        private String hex;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getHex() {
            return hex;
        }

        public void setHex(String hex) {
            this.hex = hex;
        }

    }
}
