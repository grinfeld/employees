package com.bpworld.halyavago.employees.view;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bpworld.halyavago.employees.App;
import com.bpworld.halyavago.employees.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "#LoginActivity";
    public static final String STR_RESPONSE = "response";
    public static final String LOGIN = "login";
    private String clientSecret = "k6WTx6IuUT9OAe3YfZAHWn2TRJyio27CuIiL0Cze";
    private String provider = "cashiers";
    Handler handler;

    @BindView(R.id.input_login)
    EditText emailText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        handler = new Handler(Looper.getMainLooper());
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null){
            actionBar.setElevation(0);
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        }
        emailText.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(this,R.drawable.ic_login), null);
        if (App.getToken()!=null){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }



    private void startMainActivity(){
        final String login = emailText.getText().toString();
        if (!login.isEmpty()){
            App.getApi().getOauthCashier("2",clientSecret, login, provider).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                    try {
                        if (response.isSuccessful()){
                            final String strResponse = response.body().string();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Intent intent = new Intent(LoginActivity.this, PasswordActivity.class);
                                    intent.putExtra(STR_RESPONSE, strResponse);
                                    intent.putExtra(LOGIN, login);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                                }
                            });
                        }else {
                            Log.d(TAG, "onFailure5 " + response.errorBody().string());
                            Toast.makeText(LoginActivity.this, "Пользователь не зарегистрирован", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }else {
            Toast.makeText(this, "Введите логин", Toast.LENGTH_SHORT).show();
        }
    }


    public void login() {

    }


    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {

    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Неправильный логин или пароль", Toast.LENGTH_LONG).show();

    }

    public boolean validate() {
        boolean valid = true;
        String email = emailText.getText().toString();

        if (email.isEmpty()) {
            emailText.setError("логин не может быть пустым");
            valid = false;
        } else {
            emailText.setError(null);
        }

        return valid;
    }

    public void onClickActionLogin(View view) {
        startMainActivity();
    }
}
