package com.bpworld.halyavago.employees.model;

import java.util.Date;

/**
 * Created by GrinfeldRA
 */

public class OperationItem {

    private String txt_name,
            profile_image,
            txt_disc,
            txt_total,
            txt_date;
    private Date date;


    public OperationItem(String txt_name, Date date, String profile_image, String txt_disc, String txt_total, String txt_date) {
        this.txt_name = txt_name;
        this.date = date;
        this.profile_image = profile_image;
        this.txt_disc = txt_disc;
        this.txt_total = txt_total;
        this.txt_date = txt_date;
    }

    public String getTxt_date() {
        return txt_date;
    }

    public String getTxt_name() {
        return txt_name;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public String getTxt_disc() {
        return txt_disc;
    }

    public String getTxt_total() {
        return txt_total;
    }

    public Date getDate() {
        return date;
    }
}
