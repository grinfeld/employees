package com.bpworld.halyavago.employees.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by GrinfeldRA
 */

@Table(name = "Discount",id = "_id")
public class Discount extends Model {

        public Discount() {
            super();
        }

        @Column()
        @SerializedName("id")
        @Expose
        private Integer id_discount;

        @Column
        @SerializedName("title")
        @Expose
        private String title;

        @Column
        @SerializedName("percent")
        @Expose
        private Integer value;

        @Column
        @Expose
        private Float cost;

        public Integer getIdDiscount() {
            return id_discount;
        }

        public void setId(Integer id) {
            this.id_discount = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public Float getCost() {
            return cost;
        }

        public void setCost(Float cost) {
            this.cost = cost;
        }
    }


