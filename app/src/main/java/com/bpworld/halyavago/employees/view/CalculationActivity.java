package com.bpworld.halyavago.employees.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.bpworld.halyavago.employees.App;
import com.bpworld.halyavago.employees.PicassoUtil;
import com.bpworld.halyavago.employees.R;
import com.bpworld.halyavago.employees.adapters.RecyclerViewAdapterOperationSummary;
import com.bpworld.halyavago.employees.model.CalcCheckInfo;
import com.bpworld.halyavago.employees.model.Discount;
import com.bpworld.halyavago.employees.model.OperationSummary;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by GrinfeldRA
 */

public class CalculationActivity extends AppCompatActivity{

    private static final String TAG = "#CalculationActivity";
    @BindView(R.id.profile_image)
    CircleImageView circleImageView;
    @BindView(R.id.txt_name)
    TextView txt_name;
    @BindView(R.id.txt_points)
    TextView txt_points;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private Handler handler;
    private  List<CalcCheckInfo.Discount> discountList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation);
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null){
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            View viewActionBar = getLayoutInflater().inflate(R.layout.custom_action_bar, null);
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    Gravity.CENTER);
            actionBar.setCustomView(viewActionBar, params);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            ((TextView)viewActionBar.findViewById(R.id.txt_title)).setText("Расчёт");
        }
        handler = new Handler(Looper.getMainLooper());
        String jsonString = getIntent().getStringExtra(MainActivity.CALC_CHECK_INFO);
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        Gson gson = builder.create();
        CalcCheckInfo calcCheckInfo = gson.fromJson(jsonString, CalcCheckInfo.class);
        Picasso picasso = PicassoUtil.preparePicasso(this);
        if (picasso!=null){
            picasso.load(calcCheckInfo.getUser().getPersonalInformation().getAvatar()).into(circleImageView);
        }
        txt_name.setText(String.format("%s %s", calcCheckInfo.getUser().getPersonalInformation().getFirstName(), calcCheckInfo.getUser().getPersonalInformation().getLastName()));
        txt_points.setText(String.valueOf(calcCheckInfo.getUser().getPoints())+" балов");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<RecyclerViewAdapterOperationSummary.ListItem> items = new ArrayList<>();

        items.add(new RecyclerViewAdapterOperationSummary.SummaryItem(
                String.valueOf(calcCheckInfo.getCheck().getPrice())+"Р",
                String.valueOf(calcCheckInfo.getCheck().getDiscountValue())+"Р",
                String.valueOf(calcCheckInfo.getCheck().getDiscountPrice())+"Р",
                String.valueOf(calcCheckInfo.getCheck().getDrawPointsValue())+"Б",
                String.valueOf(calcCheckInfo.getCheck().getTotalPrice())+"Р",
                String.valueOf(calcCheckInfo.getCheck().getDrawPointsPercent()),
                String.valueOf(calcCheckInfo.getCheck().getDiscountPercent())));
        recyclerView.setAdapter(new RecyclerViewAdapterOperationSummary(items, this));
    }



    @Override
    public boolean onSupportNavigateUp(){
        startActivity(new Intent(this, MainActivity.class));
        finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        return true;
    }

    public void onClickActionConfirmDiscount(View view) {
        JSONObject jsonObject = new JSONObject();
        List<Discount> execute = new Select().from(Discount.class).execute();
        for (Discount discount : execute){
            try {
                jsonObject.put(String.valueOf(discount.getIdDiscount()),discount.getCost().intValue());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        App.getApi().confirmOperation(App.BEARER+App.getToken(),getIntent().getStringExtra(MainActivity.QR),jsonObject.toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            startActivity(new Intent(CalculationActivity.this, CongratulationsActivity.class));
                            finish();
                            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                        }
                    });
                }else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strResponse = response.errorBody().string();
                                JSONObject jsonObject = new JSONObject(strResponse);
                                JSONObject errors = jsonObject.getJSONObject("errors");
                                String description = errors.getString("description");
                                String code = errors.getString("code");
                                Log.d(TAG, code+"  "+description);
                                if (code.equals("15")){
                                    startActivity(new Intent(CalculationActivity.this, NoAccessActivity.class));
                                    finish();
                                    overridePendingTransition(R.anim.push_in_up, R.anim.push_out_up);
                                }else {
                                    Toast.makeText(CalculationActivity.this, description, Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
