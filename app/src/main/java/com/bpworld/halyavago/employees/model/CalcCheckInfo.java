package com.bpworld.halyavago.employees.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by GrinfeldRA
 */

public class CalcCheckInfo {

    @SerializedName("check")
    @Expose
    private Check check;
    @SerializedName("user")
    @Expose
    private User user;

    public Check getCheck() {
        return check;
    }

    public void setCheck(Check check) {
        this.check = check;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public class Center {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("coordinates")
        @Expose
        private List<Double> coordinates = null;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<Double> getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(List<Double> coordinates) {
            this.coordinates = coordinates;
        }
    }

    public class Check {

        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("discount_value")
        @Expose
        private Integer discountValue;
        @SerializedName("discount_percent")
        @Expose
        private Integer discountPercent;
        @SerializedName("discount_price")
        @Expose
        private Integer discountPrice;
        @SerializedName("draw_points_value")
        @Expose
        private Integer drawPointsValue;
        @SerializedName("draw_points_percent")
        @Expose
        private Integer drawPointsPercent;
        @SerializedName("total_price")
        @Expose
        private Integer totalPrice;
        @SerializedName("discounts")
        @Expose
        private List<Discount> discounts = null;

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public Integer getDiscountValue() {
            return discountValue;
        }

        public void setDiscountValue(Integer discountValue) {
            this.discountValue = discountValue;
        }

        public Integer getDiscountPercent() {
            return discountPercent;
        }

        public void setDiscountPercent(Integer discountPercent) {
            this.discountPercent = discountPercent;
        }

        public Integer getDiscountPrice() {
            return discountPrice;
        }

        public void setDiscountPrice(Integer discountPrice) {
            this.discountPrice = discountPrice;
        }

        public Integer getDrawPointsValue() {
            return drawPointsValue;
        }

        public void setDrawPointsValue(Integer drawPointsValue) {
            this.drawPointsValue = drawPointsValue;
        }

        public Integer getDrawPointsPercent() {
            return drawPointsPercent;
        }

        public void setDrawPointsPercent(Integer drawPointsPercent) {
            this.drawPointsPercent = drawPointsPercent;
        }

        public Integer getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(Integer totalPrice) {
            this.totalPrice = totalPrice;
        }

        public List<Discount> getDiscounts() {
            return discounts;
        }

        public void setDiscounts(List<Discount> discounts) {
            this.discounts = discounts;
        }

    }


    public class City {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("region_id")
        @Expose
        private Integer regionId;
        @SerializedName("center")
        @Expose
        private Center center;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getRegionId() {
            return regionId;
        }

        public void setRegionId(Integer regionId) {
            this.regionId = regionId;
        }

        public Center getCenter() {
            return center;
        }

        public void setCenter(Center center) {
            this.center = center;
        }

    }

    public class Discount {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("original_price")
        @Expose
        private Double originalPrice;
        @SerializedName("discount_value")
        @Expose
        private Double discountValue;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Double getOriginalPrice() {
            return originalPrice;
        }

        public void setOriginalPrice(Double originalPrice) {
            this.originalPrice = originalPrice;
        }

        public Double getDiscountValue() {
            return discountValue;
        }

        public void setDiscountValue(Double discountValue) {
            this.discountValue = discountValue;
        }

    }

    public class PersonalInformation {

        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("sex")
        @Expose
        private Integer sex;
        @SerializedName("bdate")
        @Expose
        private String bdate;
        @SerializedName("avatar")
        @Expose
        private String avatar;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("city")
        @Expose
        private City city;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public Integer getSex() {
            return sex;
        }

        public void setSex(Integer sex) {
            this.sex = sex;
        }

        public String getBdate() {
            return bdate;
        }

        public void setBdate(String bdate) {
            this.bdate = bdate;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public City getCity() {
            return city;
        }

        public void setCity(City city) {
            this.city = city;
        }

    }

    public class User {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("parent_id")
        @Expose
        private Integer parentId;
        @SerializedName("phone_number")
        @Expose
        private Object phoneNumber;
        @SerializedName("company_points")
        @Expose
        private Double companyPoints;
        @SerializedName("nullable_points")
        @Expose
        private Double nullablePoints;
        @SerializedName("total_points")
        @Expose
        private Double totalPoints;
        @SerializedName("draw_type")
        @Expose
        private String drawType;
        @SerializedName("points")
        @Expose
        private Integer points;
        @SerializedName("personal_information")
        @Expose
        private PersonalInformation personalInformation;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Integer getParentId() {
            return parentId;
        }

        public void setParentId(Integer parentId) {
            this.parentId = parentId;
        }

        public Object getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(Object phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public Double getCompanyPoints() {
            return companyPoints;
        }

        public void setCompanyPoints(Double companyPoints) {
            this.companyPoints = companyPoints;
        }

        public Double getNullablePoints() {
            return nullablePoints;
        }

        public void setNullablePoints(Double nullablePoints) {
            this.nullablePoints = nullablePoints;
        }

        public Double getTotalPoints() {
            return totalPoints;
        }

        public void setTotalPoints(Double totalPoints) {
            this.totalPoints = totalPoints;
        }

        public String getDrawType() {
            return drawType;
        }

        public void setDrawType(String drawType) {
            this.drawType = drawType;
        }

        public Integer getPoints() {
            return points;
        }

        public void setPoints(Integer points) {
            this.points = points;
        }

        public PersonalInformation getPersonalInformation() {
            return personalInformation;
        }

        public void setPersonalInformation(PersonalInformation personalInformation) {
            this.personalInformation = personalInformation;
        }

    }
}
