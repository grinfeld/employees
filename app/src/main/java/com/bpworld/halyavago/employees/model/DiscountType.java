package com.bpworld.halyavago.employees.model;

import android.content.Intent;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by GrinfeldRA
 */

public class DiscountType {

    @SerializedName("finish")
    @Expose
    private Boolean finish;
    @SerializedName("object")
    @Expose
    private List<Discount> object = null;

    public Boolean getFinish() {
        return finish;
    }

    public void setFinish(Boolean finish) {
        this.finish = finish;
    }

    public List<Discount> getObject() {
        return object;
    }

    public void setObject(List<Discount> object) {
        this.object = object;
    }


}
