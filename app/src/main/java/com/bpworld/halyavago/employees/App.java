package com.bpworld.halyavago.employees;

import android.content.Context;
import android.content.SharedPreferences;
import com.bpworld.halyavago.employees.iface.Api;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import io.fabric.sdk.android.Fabric;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by GrinfeldRA
 */
public class App extends com.activeandroid.app.Application{

    private static Api api;
    private static final String APP_PREFERENCES = "mysettings";
    private static final String TOKEN = "token";
    private static final String QR_CODE = "qr_code";
    private static SharedPreferences mSettings;
    public static String BEARER = "Bearer ";

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        OkHttpClient client = new OkHttpClient();
        try {
            client = new OkHttpClient.Builder()
                    .addInterceptor(new ConnectivityInterceptor(this))
                    .sslSocketFactory(new TLSSocketFactory()).build();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://xlgo.ru")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        api = retrofit.create(Api.class);

    }

    public static void setQrCode(String qrCode){
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(QR_CODE, qrCode);
        editor.apply();
    }

    public static String getQrCode(){
        return mSettings.getString(QR_CODE, null);
    }

    public static void setToken(String token){
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(TOKEN, token);
        editor.apply();
    }

    public static String getToken(){
        return mSettings.getString(TOKEN, null);
    }

    public static Api getApi() {
        return api;
    }

}
