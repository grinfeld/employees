package com.bpworld.halyavago.employees.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.bpworld.halyavago.employees.App;
import com.bpworld.halyavago.employees.R;
import com.bpworld.halyavago.employees.adapters.RecyclerViewAdapterAddDiscount;
import com.bpworld.halyavago.employees.model.Discount;
import com.bpworld.halyavago.employees.model.DiscountType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by GrinfeldRA
 */

public class AddDiscountActivity extends AppCompatActivity implements RecyclerViewAdapterAddDiscount.OnItemCheckListener {

    private static final String TAG = "#AddDiscountActivity";
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private List<Discount> data = new ArrayList<>();
    private Handler handler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_discount);
        ButterKnife.bind(this);
        handler = new Handler(Looper.getMainLooper());
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null){
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            View viewActionBar = getLayoutInflater().inflate(R.layout.custom_action_bar, null);
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    Gravity.CENTER);
            actionBar.setCustomView(viewActionBar, params);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            ((TextView)viewActionBar.findViewById(R.id.txt_title)).setText("Добавить Скидку");
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(this));//8qFDbleE

        App.getApi().getDiscountType(App.BEARER+App.getToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        String strResponse = response.body().string();
                        Log.d(TAG, "isSuccessful: " +strResponse);
                        JSONObject jsonObject = new JSONObject(strResponse);
                        String jsonString = jsonObject.getString("data");
                        GsonBuilder builder = new GsonBuilder();
                        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
                        Gson gson = builder.create();
                        final DiscountType discountType = gson.fromJson(jsonString, DiscountType.class);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                List<Discount> discountList = discountType.getObject();
                                List<Discount> execute = new Select().from(Discount.class).execute();
                                Iterator<Discount> iter = discountList.iterator();
                                while(iter.hasNext()) {
                                    Discount discount = iter.next();
                                    int id = discount.getIdDiscount();
                                    for (Discount discount1 : execute){
                                        if(id==discount1.getIdDiscount()) {
                                            iter.remove();
                                            break;
                                        }
                                    }
                                }
                                recyclerView.setAdapter(new RecyclerViewAdapterAddDiscount(discountList,
                                        AddDiscountActivity.this,AddDiscountActivity.this));
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    try {
                        String strResponse = response.errorBody().string();
                        Log.d(TAG, "errorBody: " +strResponse);
                        JSONObject jsonObject = new JSONObject(strResponse);
                        JSONObject errors = jsonObject.getJSONObject("errors");
                        String code = errors.getString("code");
                        if (code.equals("15")){
                            startActivity(new Intent(AddDiscountActivity.this, NoAccessActivity.class));
                            finish();
                            overridePendingTransition(R.anim.push_in_up, R.anim.push_out_up);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "errorBody: " +t.getMessage());
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        startActivity(new Intent(this, MainActivity.class));
        finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        return true;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private boolean checkNullDiscountCost(){
        for (Discount discount : data){
            if (discount.getCost()==null){
                return true;
            }
        }
        return false;
    }

    public void onClickActionAdd(View view) {
        if (checkNullDiscountCost()){
            Toast.makeText(this, "Введите сумму", Toast.LENGTH_LONG).show();
            return;
        }
        for (Discount discount : data){
            discount.save();
        }
        setResult(RESULT_OK);
        startActivity(new Intent(this, MainActivity.class));
        finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }


    private boolean checkExistDiscount(Discount item){
        for (Discount discount : data){
            if (discount.getIdDiscount() == item.getIdDiscount()){
                return true;
            }
        }
        return false;
    }

    @Override
    public void onItemCheck(Discount item) {
        if (checkExistDiscount(item)){
            for (Discount discount : data){
                if (discount.getIdDiscount()==item.getIdDiscount()){
                    discount.setCost(item.getCost());
                    break;
                }
            }
        }else {
            data.add(item);
        }
    }

    @Override
    public void onItemUnCheck(Discount item) {
        data.remove(item);
    }
}
