package com.bpworld.halyavago.employees.iface;
import com.bpworld.halyavago.employees.model.OauthToken;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by GrinfeldRA
 */

public interface Api {

    @FormUrlEncoded
    @POST("oauth/login")
    Call<OauthToken> getOauthToken(@Field("client_id") String client_id,
                                   @Field("client_secret") String client_secret,
                                   @Field("email") String email,
                                   @Field("password") String password,
                                   @Field("provider") String provider);
    @FormUrlEncoded
    @POST("cashier")
    Call<ResponseBody> getOauthCashier(@Field("client_id") String client_id,
                                    @Field("client_secret") String client_secret,
                                    @Field("email") String email,
                                    @Field("provider") String provider);

    @GET("api/cashier/me")
    Call<ResponseBody> getInfoCashier(@Header("Authorization")String token);

    @Multipart
    @POST("api/cashier/upload_avatar")
    Call<ResponseBody> uploadAvatar(@Header("Authorization")String token, @Part()MultipartBody.Part file);

    @GET("api/cashier/discount_types")
    Call<ResponseBody> getDiscountType(@Header("Authorization")String token);

    @FormUrlEncoded
    @POST("api/cashier/calc_check")
    Call<ResponseBody> calcCheck(@Header("Authorization")String token,
                                 @Field("operation_token") String operation_token,
                                 @Field("discounts") String discounts);

    @FormUrlEncoded
    @POST("api/cashier/confirm_operation")
    Call<ResponseBody> confirmOperation(@Header("Authorization")String token,
                                        @Field("operation_token") String operation_token,
                                        @Field("discounts") String discounts);


    @GET("api/operations")
    Call<ResponseBody> getOperations(@Header("Authorization")String token,
                                     @Query("cashier_id") int cashier_id,
                                     @Query("company_id")int company_id,
                                     @Query("date") String date);

}
